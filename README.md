# Eye Protector

![pic](http://splashingspray.com/wp-content/uploads/2019/05/Eye-Protector-Demo.gif)

 

A tool that can adjust the screen brightness, contrast and color temperature.

Click on the icon in the task tray to show/hide the tool. Click on the "X" to exit the tool.

*This tool may not work if the monitor's DDC/CI option is disabled (causes I2C bus error). [More information](https://appuals.com/what-is-ddc-ci-and-how-to-use-it/)*

## System requirements

* Windows Vista or newer

* .NET Framework 4.5.1 or newer

* For development: Visual Studio 2017 or newer