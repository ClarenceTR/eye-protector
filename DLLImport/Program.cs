﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Windows.Forms;

namespace EyeProtector
{
    class Program
    {
        const int PHYSICAL_MONITOR_DESCRIPTION_SIZE = 128;
        const int MONITOR_DEFAULTTONEAREST = 0x00000002;

        public const int ERROR_GEN_FAILURE = 0x0000001F;

        #region Win API structs & enums

        [Flags]
        public enum MC_COLOR_TEMPERATURE
        {
            MC_COLOR_TEMPERATURE_UNKNOWN,
            MC_COLOR_TEMPERATURE_4000K,
            MC_COLOR_TEMPERATURE_5000K,
            MC_COLOR_TEMPERATURE_6500K,
            MC_COLOR_TEMPERATURE_7500K,
            MC_COLOR_TEMPERATURE_8200K,
            MC_COLOR_TEMPERATURE_9300K,
            MC_COLOR_TEMPERATURE_10000K,
            MC_COLOR_TEMPERATURE_11500K
        };

        public static string TranslateColorTemperature(int temperature)
        {
            switch (temperature)
            {
                case 0x00000001: return "4000K";
                case 0x00000002: return "5000K";
                case 0x00000003: return "6500K";
                case 0x00000004: return "7500K";
                case 0x00000005: return "8200K";
                case 0x00000006: return "9300K";
                case 0x00000007: return "10000K";
                case 0x00000008: return "11500K";
                default:
                    return "Unknown temperature.";
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PHYSICAL_MONITOR
        {
            public IntPtr hPhysicalMonitor;
            [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U2, SizeConst = PHYSICAL_MONITOR_DESCRIPTION_SIZE)]
            public char[] szPhysicalMonitorDescription;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            int x;
            int y;
        }

        #endregion

        #region WinAPI functions

        // I'm not using Message Box in this program but that's a Microsoft Doc tutorial of how to use DllImport.
        // Use DllImport to import the Win32 MessageBox function.
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int MessageBox(IntPtr hWnd, string text, string caption, uint type);


        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool GetPhysicalMonitorsFromHMONITOR(IntPtr hMonitor, uint dwPhysicalMonitorArraySize, [Out] PHYSICAL_MONITOR[] pPhysicalMonitorArray);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool GetNumberOfPhysicalMonitorsFromHMONITOR(IntPtr hMonitor, out uint pdwNumberOfPhysicalMonitors);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool DestroyPhysicalMonitors(uint dwPhysicalMonitorArraySize, PHYSICAL_MONITOR[] pPhysicalMonitorArray);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool GetCursorPos(out POINT lpPoint);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr MonitorFromPoint(POINT pt, uint dwFlags);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool GetMonitorBrightness(IntPtr hMonitor, out uint pdwMinimumBrightness, out uint pdwCurrentBrightness, out uint pdwMaximumBrightness);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool SetMonitorBrightness(IntPtr hMonitor, uint dwNewBrightness);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool GetMonitorContrast(IntPtr hMonitor, out uint pdwMinimumContrast, out uint pdwCurrentContrast, out uint pdwMaximumContrast);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool SetMonitorContrast(IntPtr hMonitor, uint dwNewContrast);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool GetMonitorColorTemperature(IntPtr hMonitor, out MC_COLOR_TEMPERATURE pctCurrentColorTemperature);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool SetMonitorColorTemperature(IntPtr hMonitor, MC_COLOR_TEMPERATURE ctCurrentColorTemperature);

        [DllImport("Dxva2.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool GetMonitorCapabilities(IntPtr hMonitor, out uint pdwMonitorCapabilities, out uint pdwSupportedColorTemperatures);

        #endregion

        public static MC_COLOR_TEMPERATURE GetMonitorColorTemperature(PHYSICAL_MONITOR physicalMonitor)
        {
            MC_COLOR_TEMPERATURE ctCurrentColorTemperature;
            if (!GetMonitorColorTemperature(physicalMonitor.hPhysicalMonitor, out ctCurrentColorTemperature))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            return ctCurrentColorTemperature;
        }

        public static void SetMonitorColorTemperature(PHYSICAL_MONITOR physicalMonitor, MC_COLOR_TEMPERATURE ctCurrentColorTemperature)
        {
            if (!SetMonitorColorTemperature(physicalMonitor.hPhysicalMonitor, ctCurrentColorTemperature))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        public static IntPtr GetCurrentMonitor()
        {
            POINT point = new POINT();
            if (!GetCursorPos(out point))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            return MonitorFromPoint(point, MONITOR_DEFAULTTONEAREST);
        }

        public static PHYSICAL_MONITOR[] GetPhysicalMonitors(IntPtr hMonitor)
        {
            uint dwNumberOfPhysicalMonitors;
            if (!GetNumberOfPhysicalMonitorsFromHMONITOR(hMonitor, out dwNumberOfPhysicalMonitors))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            PHYSICAL_MONITOR[] physicalMonitorArray = new PHYSICAL_MONITOR[dwNumberOfPhysicalMonitors];
            if (!GetPhysicalMonitorsFromHMONITOR(hMonitor, dwNumberOfPhysicalMonitors, physicalMonitorArray))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            return physicalMonitorArray;
        }

        public static void DestroyPhysicalMonitors(PHYSICAL_MONITOR[] physicalMonitorArray)
        {
            if (!DestroyPhysicalMonitors((uint)physicalMonitorArray.Length, physicalMonitorArray))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        public static double GetMonitorBrightness(PHYSICAL_MONITOR physicalMonitor)
        {
            uint dwMinimumBrightness, dwCurrentBrightness, dwMaximumBrightness;
            if (!GetMonitorBrightness(physicalMonitor.hPhysicalMonitor, out dwMinimumBrightness, out dwCurrentBrightness, out dwMaximumBrightness))
            {
                // Some monitors (e.g. View Sonic) may fail on the first try.
                // Get it again.
                if (!GetMonitorBrightness(physicalMonitor.hPhysicalMonitor, out dwMinimumBrightness, out dwCurrentBrightness, out dwMaximumBrightness))
                    throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            return (double)(dwCurrentBrightness - dwMinimumBrightness) / (double)(dwMaximumBrightness - dwMinimumBrightness);
        }

        public static void SetMonitorBrightness(PHYSICAL_MONITOR physicalMonitor, double brightness)
        {
            uint dwMinimumBrightness, dwCurrentBrightness, dwMaximumBrightness;
            if (!GetMonitorBrightness(physicalMonitor.hPhysicalMonitor, out dwMinimumBrightness, out dwCurrentBrightness, out dwMaximumBrightness))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            if (!SetMonitorBrightness(physicalMonitor.hPhysicalMonitor, (uint)(dwMinimumBrightness + (dwMaximumBrightness - dwMinimumBrightness) * brightness)))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        public static double GetMonitorContrast(PHYSICAL_MONITOR physicalMonitor)
        {
            uint dwMinimumContrast, dwCurrentContrast, dwMaximumContrast;
            if (!GetMonitorContrast(physicalMonitor.hPhysicalMonitor, out dwMinimumContrast, out dwCurrentContrast, out dwMaximumContrast))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            return (double)(dwCurrentContrast - dwMinimumContrast) / (double)(dwMaximumContrast - dwMinimumContrast);
        }

        public static void SetMonitorContrast(PHYSICAL_MONITOR physicalMonitor, double contrast)
        {
            uint dwMinimumContrast, dwCurrentContrast, dwMaximumContrast;
            if (!GetMonitorContrast(physicalMonitor.hPhysicalMonitor, out dwMinimumContrast, out dwCurrentContrast, out dwMaximumContrast))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            if (!SetMonitorContrast(physicalMonitor.hPhysicalMonitor, (uint)(dwMinimumContrast + (dwMaximumContrast - dwMinimumContrast) * contrast)))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        #region Utility functions

        public static T Clamp<T>(T value, T min, T max) where T : IComparable
        {
            var v = value;
            if (v.CompareTo(max) > 0) v = max;
            if (v.CompareTo(min) < 0) v = min;
            return v;
        }

        // Not used yet.
        public struct Color
        {
            public int r;
            public int g;
            public int b;
        }

        // Not used yet.
        public static Color GetColor(int temperature)
        {
            // Clamp temperature in [1000, 40000]
            temperature = Clamp(temperature, 1000, 40000);
            //if (temperature > 40000) temperature = 40000;
            //if (temperature < 1000) temperature = 1000;

            double t = temperature;
            t /= 100.0;
            Color res = new Color();

            // r
            if (t <= 66) res.r = 255;
            else
            {
                var tmpCalc = t - 60.0;
                tmpCalc = 329.698727446 * Math.Pow(tmpCalc, -0.1332047592);
                res.r = (int)tmpCalc;
                // Clamp r
                res.r = Clamp(res.r, 0, 255);
            }

            // g
            if (t <= 66)
            {
                var tmpCalc = t;
                tmpCalc = 99.4708025861 * Math.Log(tmpCalc) - 161.1195681661;
                res.g = (int)tmpCalc;
                res.g = Clamp(res.g, 0, 255);
            }
            else
            {
                var tmpCalc = t - 60;
                tmpCalc = 288.1221695283 * Math.Pow(tmpCalc, -0.0755148492);
                res.g = Clamp(res.g, 0, 255);
            }

            // b
            if (t >= 66) res.b = 255;
            else if (t <= 19) res.b = 0;
            else
            {
                var tmpCalc = t - 10;
                tmpCalc = 138.5177312231 * Math.Log(tmpCalc) - 305.0447927307;
                res.b = (int)tmpCalc;
                res.b = Clamp(res.b, 0, 255);
            }

            // return
            return res;
        }

        #endregion

        #region Functions and fields that inquire if the monitor supports the functions. Not used.

        private static uint GetMonitorCapabilities(PHYSICAL_MONITOR physicalMonitor)
        {
            uint dwMonitorCapabilities, dwSupportedColorTemperatures;

            // The function fails if the monitor does not support DDC/CI.
            if (!GetMonitorCapabilities(physicalMonitor.hPhysicalMonitor, out dwMonitorCapabilities, out dwSupportedColorTemperatures))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            return dwMonitorCapabilities;
        }

        public static bool DoesSupportBrightness(PHYSICAL_MONITOR physicalMonitor)
        {
            return (GetMonitorCapabilities(physicalMonitor) & MC_CAPS_BRIGHTNESS) != 0;
        }

        public static bool DoesSupportContrast(PHYSICAL_MONITOR physicalMonitor)
        {
            return (GetMonitorCapabilities(physicalMonitor) & MC_CAPS_CONTRAST) != 0;
        }

        public static bool DoesSupportColorTemperature(PHYSICAL_MONITOR physicalMonitor)
        {
            return (GetMonitorCapabilities(physicalMonitor) & MC_CAPS_COLOR_TEMPERATURE) != 0;
        }

        public const uint MC_CAPS_NONE = 0x00000000;
        public const uint MC_CAPS_MONITOR_TECHNOLOGY_TYPE = 0x00000001;
        public const uint MC_CAPS_BRIGHTNESS = 0x00000002;
        public const uint MC_CAPS_CONTRAST = 0x00000004;
        public const uint MC_CAPS_COLOR_TEMPERATURE = 0x00000008;
        public const uint MC_CAPS_RED_GREEN_BLUE_GAIN = 0x00000010;
        public const uint MC_CAPS_RED_GREEN_BLUE_DRIVE = 0x00000020;
        public const uint MC_CAPS_DEGAUSS = 0x00000040;
        public const uint MC_CAPS_DISPLAY_AREA_POSITION = 0x00000080;
        public const uint MC_CAPS_DISPLAY_AREA_SIZE = 0x00000100;
        public const uint MC_CAPS_RESTORE_FACTORY_DEFAULTS = 0x00000400;
        public const uint MC_CAPS_RESTORE_FACTORY_COLOR_DEFAULTS = 0x00000800;
        public const uint MC_RESTORE_FACTORY_DEFAULTS_ENABLES_MONITOR_SETTINGS = 0x00001000;

        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_NONE = 0x00000000;
        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_4000K = 0x00000001;
        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_5000K = 0x00000002;
        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_6500K = 0x00000004;
        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_7500K = 0x00000008;
        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_8200K = 0x00000010;
        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_9300K = 0x00000020;
        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_10000K = 0x00000040;
        public const uint MC_SUPPORTED_COLOR_TEMPERATURE_11500K = 0x00000080;

        #endregion

        static void Main(string[] args)
        {
            Application.Run(new FormMain());
        }
    }
}
