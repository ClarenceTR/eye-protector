﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EyeProtector
{
    public partial class FormMain : Form
    {
        Program.PHYSICAL_MONITOR[] m_physicalMonitors;

        public FormMain()
        {
            InitializeComponent();
            m_physicalMonitors = Program.GetPhysicalMonitors(Program.GetCurrentMonitor());
            var b = Program.GetMonitorBrightness(m_physicalMonitors[0]);
            var c = Program.GetMonitorContrast(m_physicalMonitors[0]);
            tbBrightness.Value = (int)(b * tbBrightness.Maximum);
            tbContrast.Value = (int)(c * tbContrast.Maximum);
            tbTemp.Value = (int)Program.GetMonitorColorTemperature(m_physicalMonitors[0]);
            txtTemp.Text = Program.TranslateColorTemperature(tbTemp.Value);

            StartPosition = FormStartPosition.CenterScreen;
        }

        private void RefreshBrightness()
        {
            foreach (Program.PHYSICAL_MONITOR physicalMonitor in m_physicalMonitors)
            {
                try
                {
                    Program.SetMonitorBrightness(physicalMonitor, (double)tbBrightness.Value / tbBrightness.Maximum);
                }
                catch (Win32Exception ex)
                {
                    // LG Flatron W2443T may throw exceptions when rapidly changing brightness or contrast
                    if (ex.NativeErrorCode == Program.ERROR_GEN_FAILURE)
                    {
                        Console.WriteLine("ERROR_GEN_FAILURE while setting brightness.");
                        break;
                    }
                    else
                        throw;
                }
            }
        }

        void RefreshContrast()
        {
            foreach (Program.PHYSICAL_MONITOR physicalMonitor in m_physicalMonitors)
            {
                try
                {
                    Program.SetMonitorContrast(physicalMonitor, (double)tbContrast.Value / tbContrast.Maximum);
                }
                catch (Win32Exception ex)
                {
                    // LG Flatron W2443T may throw exceptions when rapidly changing brightness or contrast
                    if (ex.NativeErrorCode == Program.ERROR_GEN_FAILURE)
                    {
                        Console.WriteLine("ERROR_GEN_FAILURE while setting contrast.");
                        break;
                    }
                    else
                        throw;
                }
            }
        }

        void RefreshTemperature()
        {
            foreach (Program.PHYSICAL_MONITOR physicalMonitor in m_physicalMonitors)
            {
                try
                {
                    Program.SetMonitorColorTemperature(physicalMonitor, (Program.MC_COLOR_TEMPERATURE)tbTemp.Value);
                }
                catch (Win32Exception ex)
                {
                    throw ex;
                }
            }

            txtTemp.Text = Program.TranslateColorTemperature(tbTemp.Value);
        }

        #region Form Functions

        private void tbBrightness_Scroll(object sender, EventArgs e)
        {
            RefreshBrightness();
        }

        private void tbContrast_Scroll(object sender, EventArgs e)
        {
            RefreshContrast();
        }

        private void tbTemp_Scroll(object sender, EventArgs e)
        {
            RefreshTemperature();
        }

        bool m_shown = true;
        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (lastDeactivateValid && Environment.TickCount - lastDeactivateTick < 500) return;
            this.Show();
            this.Activate();
            lastDeactivateValid = false;
        }

        #endregion

        private void FormMain_Deactivate(object sender, EventArgs e)
        {
            lastDeactivateTick = Environment.TickCount;
            lastDeactivateValid = true;
            this.Hide();
        }

        int lastDeactivateTick;
        bool lastDeactivateValid;
    }
}
